from item import Item


def setItemQuality(item, quality, min_quality=0, max_quality=50):
    item.quality = max(0, min(50, quality))


class GildedRose(object):

    def __init__(self, items):
        self.items = items

    def update_quality(self):
        for item in self.items:
            if item.name == "Sulfuras, Hand of Ragnaros":
                quality_modifier = 0
            elif item.name == "Backstage passes to a TAFKAL80ETC concert":
                if item.sell_in <= 0:
                    quality_modifier = -item.quality
                elif item.sell_in < 6:
                    quality_modifier = 3
                elif item.sell_in < 11:
                    quality_modifier = 2
                else:
                    quality_modifier = 1
            elif item.name == "Aged Brie":
                quality_modifier = 2 if item.sell_in <= 0 else 1
            else:
                quality_modifier = -2 if item.sell_in <= 0 else -1
                if item.name.startswith("Conjured"):
                    quality_modifier *= 2

            if item.name != "Sulfuras, Hand of Ragnaros":
                item.sell_in = item.sell_in - 1
                setItemQuality(item, item.quality + quality_modifier)
