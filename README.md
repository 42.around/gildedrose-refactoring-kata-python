# Gilded Rose Refactoring Kata

Gilded Rose kata solved using Python. 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Python and `pip` are required to use this project. More information in the [built with section](#built-with).

### Installing

After cloning this project, simply run

```
make install
```

And project should download all required dependencies. You can test if it installed correctly by [running the tests](#running-the-tests)

## Running the tests

You can execute all project tests by running
```
make test
```

It runs [style test](#style-test), [fixture test](#fixture-test) and [coverage-test](#coverage-test).

### Style test

Style test is responsible for maintaining consistent code style and ensuring that project is running without issues.

If you want to check code style only, run

```
make test-style
```

### Fixture test

Fixture tests are required to determine whether code is fit for use. You can launch fixture tests by running

```
make test-fixture
```

### Coverage test
Coverage tests are required to determine how much code is used during tests. You can launch coverage tests by running
```
make test-coverage
```

## Built With

* [Python](https://www.python.org/) - Interpreter
* [pip](https://pypi.org/project/pip/) - Package installer

## Authors

* **Tomas Markevičius** - *Initial work* - [Tomas Markevičius](https://gitlab.com/42.around)

## License

This project is licensed under the MIT license - see the [LICENSE.md](LICENSE.md) file for details
