install:
	pip install -Ur requirements.txt

clean:
	find . -name '*.pyc' -exec rm --force {} +
	find . -name '.coverage' -exec rm --force {} +

test: test-style test-fixture test-coverage

test-fixture: clean
	pytest test_text_fixture.py

test-coverage: clean
	pytest --cov=./gilded_rose/ --cov-report term-missing # this executes all tests in the root directory

test-style: clean
	pycodestyle gilded_rose
