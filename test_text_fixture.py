# -*- coding: utf-8 -*-
from __future__ import print_function
import sys
import pytest

sys.path.insert(1, 'gilded_rose')
from item import Item
from gilded_rose import GildedRose

@pytest.fixture
def example_shop_items():
    return [
        Item(name="+5 Dexterity Vest", sell_in=10, quality=20),
        Item(name="+1 Dexterity Vest", sell_in=1, quality=4),
        Item(name="Aged Brie", sell_in=1, quality=0),
        Item(name="Elixir of the Mongoose", sell_in=5, quality=7),
        Item(name="Sulfuras, Hand of Ragnaros", sell_in=-1, quality=80),
        Item(name="Sulfuras, Hand of Ragnaros", sell_in=1, quality=80),
        Item(name="Backstage passes to a TAFKAL80ETC concert", sell_in=15, quality=20),
        Item(name="Backstage passes to a TAFKAL80ETC concert", sell_in=10, quality=49),
        Item(name="Backstage passes to a TAFKAL80ETC concert", sell_in=1, quality=40),
        Item(name="Conjured Mana Cake", sell_in=1, quality=6),
    ]

def test_first_day(example_shop_items):
    gilded_rose = GildedRose(example_shop_items)
    gilded_rose.update_quality()
    assert [repr(item) for item in example_shop_items] == [
        "+5 Dexterity Vest, 9, 19",
        "+1 Dexterity Vest, 0, 3",
        "Aged Brie, 0, 1",
        "Elixir of the Mongoose, 4, 6",
        "Sulfuras, Hand of Ragnaros, -1, 80",
        "Sulfuras, Hand of Ragnaros, 1, 80",
        "Backstage passes to a TAFKAL80ETC concert, 14, 21",
        "Backstage passes to a TAFKAL80ETC concert, 9, 50",
        "Backstage passes to a TAFKAL80ETC concert, 0, 43",
        "Conjured Mana Cake, 0, 4",
    ]

def test_second_day(example_shop_items):
    gilded_rose = GildedRose(example_shop_items)

    gilded_rose.update_quality()
    gilded_rose.update_quality()

    assert [repr(item) for item in example_shop_items] == [
        "+5 Dexterity Vest, 8, 18",
        "+1 Dexterity Vest, -1, 1",
        "Aged Brie, -1, 3",
        "Elixir of the Mongoose, 3, 5",
        "Sulfuras, Hand of Ragnaros, -1, 80",
        "Sulfuras, Hand of Ragnaros, 1, 80",
        "Backstage passes to a TAFKAL80ETC concert, 13, 22",
        "Backstage passes to a TAFKAL80ETC concert, 8, 50",
        "Backstage passes to a TAFKAL80ETC concert, -1, 0",
        "Conjured Mana Cake, -1, 0",
    ]
